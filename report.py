import os
import telegram
import shutil
import pandas as pd
from telegram.ext import *
from logging import basicConfig, getLogger, INFO

basicConfig(level=INFO)
log = getLogger()

TOKEN = '6411710440:AAEVjnR8wWBqN5qeuYudCsDvxokBTCW4O54'
updater = Updater(TOKEN, use_context = True)
dp = updater.dispatcher
bot = telegram.Bot(token=TOKEN)

url = ''


def start_command(update, context):
    name = update.message.chat.first_name
    update.message.reply_text("Hello " + name )
    update.message.reply_text("Please enter the link to your report")
    print("_______________________________________________________") 
    print("New User: " + name)
    print("_______________________________________________________") 

          
def link_handler(update, context):
    idd = update.message.chat_id
    global url
    url = update.message.text
    name = update.message.chat.first_name

    update.message.reply_text("Please wait " + name + "...")

    
    month_number = 5
    idd = update.message.chat_id
    
    
    print("_______________________________________________________")
    print(name + " is Waiting Now")
    bot.send_message(chat_id=602083684, text = '---' + name + '--- requst report')
    

    sheet_url = url
    csv_export_url = sheet_url.replace('/edit#gid=', '/export?format=csv&gid=')
    df = pd.read_csv(csv_export_url, header=None)
    df.columns = ["date", "name", "nash" , "catg", "link" , "type", "source"]
    df = df[["nash" , "catg" , "type", "source"]]
    ####
    df_new = df[df['type']=='جديد']
    df_update = df[df['type']=='تحديث']
    ####
    total_new_profiles = len(df_new)
    total_updated_profiles = len(df_update)
    total_profiles = total_new_profiles + total_updated_profiles
    ####
    df_new_source = df_new['source'].value_counts().reset_index()
    df_new_source.columns = ['source', 'counts']
    df_update_source = df_update['source'].value_counts().reset_index()
    df_update_source.columns = ['source', 'counts']
    #### Nashonailty
    df_new_nash = df_new['nash'].value_counts().reset_index()
    df_new_nash.columns = ['nash', 'counts']
    df_update_nash = df_update['nash'].value_counts().reset_index()
    df_update_nash.columns = ['nash', 'counts']
    ####
    df_new_catg = df_new['catg'].value_counts().reset_index()
    df_new_catg.columns = ['catg', 'counts']
    df_update_catg = df_update['catg'].value_counts().reset_index()
    df_update_catg.columns = ['catg', 'counts']

    new_final_name = 'final'+name+'.xlsx'
    shutil.copy('final.xlsx', new_final_name)


    month_names = {1:'يناير',2:'فبراير',3:'مارس',4:'أبريل',5:'مايو',6:'يونيو',7:'يوليو',8:'أغسطس',9:'سبتمتبر',10:'أكتوبر',11:'نوفمبر',12:'ديسمبر'}

    from openpyxl import load_workbook
    workbook = load_workbook(filename=new_final_name)
    sht = workbook.sheetnames
    sheet = workbook[sht[0]]
    month = "أبريل"
    year = 2024
    namerep = name
    month = "تقرير عمل شهر " + month_names[month_number] + " " + str(year)

    #open workbook
    sheet['C1'] = month
    sheet['A2'] = namerep
    sheet["B4"] = total_profiles
    sheet["B6"] = total_new_profiles
    sheet["D6"] = total_updated_profiles
    workbook.save(filename=new_final_name)

    ###
    num=14
    varo=False
    for j in range(0,len(df_new_source)):
        for i in range(7,14):
            x = sheet["A"+str(i)]
            if df_new_source.iloc[j,0] == x.value:
                varo=True
                sheet["B"+str(i)] = df_new_source.iloc[j,1]
        if varo == False:
            sheet["A"+str(num)]=df_new_source.iloc[j,0]
            sheet["B"+str(num)]=df_new_source.iloc[j,1]
            num=num+1
        else:
            varo=False

        workbook.save(filename=new_final_name)

    num=14
    varo=False
    for j in range(0,len(df_update_source)):
        for i in range(7,14):
            x = sheet["C"+str(i)]
            if df_update_source.iloc[j,0] == x.value:
                varo=True
                sheet["D"+str(i)] = df_update_source.iloc[j,1]

        if varo == False:
            sheet["C"+str(num)]=df_update_source.iloc[j,0]
            sheet["D"+str(num)]=df_update_source.iloc[j,1]
            num=num+1
        else:
            varo=False

        workbook.save(filename=new_final_name)

    from difflib import SequenceMatcher

    def similar(a, b):
        return SequenceMatcher(None, a, b).ratio()

    #NEW-nash
    non=49
    varfn=False
    for j in range(0,len(df_new_nash)):
        for i in range(20,49):
            x = sheet["A"+str(i)]
            #if similar(df_new_nash.iloc[j,0],x.value) > 0.769 or (df_new_nash.iloc[j,0] == x.value):
            if (df_new_nash.iloc[j,0] == x.value):
                varfn=True
                sheet["B"+str(i)] = df_new_nash.iloc[j,1]
                workbook.save(filename=new_final_name)
            
        if varfn == False:
            sheet["A"+str(non)]=df_new_nash.iloc[j,0]
            sheet["B"+str(non)]=df_new_nash.iloc[j,1]
            workbook.save(filename=new_final_name)
            non=non+1
        else:
            varfn=False
            
    

    #Updated-nash
    non=49
    varfu=False
    for j in range(0,len(df_update_nash)):
        for i in range(20,49):
            x = sheet["F"+str(i)]
            #if similar(df_update_nash.iloc[j,0],x.value) > 0.769 or (df_update_nash.iloc[j,0] == x.value):
            if (df_update_nash.iloc[j,0] == x.value):
                varfu=True
                sheet["G"+str(i)] = df_update_nash.iloc[j,1]
                workbook.save(filename=new_final_name)
            
        if varfu == False:
            sheet["F"+str(non)]=df_update_nash.iloc[j,0]
            sheet["G"+str(non)]=df_update_nash.iloc[j,1]
            workbook.save(filename=new_final_name)
            non=non+1
        else:
            varfu=False

    
    #NEW-Classment      
    nonncl=71
    varfncl=False
    for j in range(0,len(df_new_catg)):
        for i in range(20,71):
            x = sheet["C"+str(i)]
            if df_new_catg.iloc[j,0] == x.value:
                varfncl=True
                sheet["D"+str(i)] = df_new_catg.iloc[j,1]
            
        if varfncl == False:
            sheet["C"+str(nonncl)]=df_new_catg.iloc[j,0]
            sheet["D"+str(nonncl)]=df_new_catg.iloc[j,1]
            nonncl=nonncl+1
        else:
            varfncl=False

    workbook.save(filename=new_final_name)
            
    #CHANGED-Classment
    nonccl=71
    varfccl=False
    for j in range(0,len(df_update_catg)):
        for i in range(20,71):
            x = sheet["H"+str(i)]
            if df_update_catg.iloc[j,0] == x.value:
                varfccl=True
                sheet["I"+str(i)] = df_update_catg.iloc[j,1]
            
        if varfccl == False:
            sheet["H"+str(nonccl)]=df_update_catg.iloc[j,0]
            sheet["I"+str(nonccl)]=df_update_catg.iloc[j,1]
            nonccl=nonccl+1
        else:
            varfccl=False

    workbook.save(filename=new_final_name)

    ####SECOND SHEET####

    heshe1 = df[df['source'] == "طلب من الشخصية"]
    heshe2 =  df[df['source'] == "طلب تحديث من فورم الموقع"]
    heshe = pd.concat([heshe1,heshe2])
    nash_heshe = heshe['nash'].value_counts()
    nash_heshe = pd.DataFrame(nash_heshe)
    nash_heshe.reset_index(inplace=True)
    listo = nash_heshe.rename(columns = {'index':'Nationalities','nash':'Numbers'})


    shet = workbook[sht[1]]
    dictt = {1:'B',2:'C',3:'D',4:'E',5:'F',6:'G',7:'H',8:'I',9:'J',10:'K',11:'L',12:'M'}
    mmonth = dictt[month_number]

    roe = 21

    for i in range(2,20):
        shet[mmonth+str(i)] = 0
        workbook.save(filename=new_final_name)

    for i in range(0,listo.count()[0]):
        if listo.iloc[i,0] == "سعودي":
            shet[mmonth+'2'] =listo.iloc[i,1]
        elif listo.iloc[i,0] == "مصري":
            shet[mmonth+'3'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "إماراتي":
            shet[mmonth+'4'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "عماني":
            shet[mmonth+'5'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "كويتي":
            shet[mmonth+'6'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "قطري":
            shet[mmonth+'7'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "عراقي":
            shet[mmonth+'8'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "سوري":
            shet[mmonth+'9'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "أردني":
            shet[mmonth+'10'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "لبناني":
            shet[mmonth+'11'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "بحريني":
            shet[mmonth+'12'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "سوداني":
            shet[mmonth+'13'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "مغربي":
            shet[mmonth+'14'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "فلسطيني":
            shet[mmonth+'15'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "جزائري":
            shet[mmonth+'16'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "ليبي":
            shet[mmonth+'17'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "تونسي":
            shet[mmonth+'18'] = listo.iloc[i,1]
        elif listo.iloc[i,0] == "يمني":
            shet[mmonth+'19'] = listo.iloc[i,1]
        else:
            shet['A'+str(roe)] = listo.iloc[i,0]
            shet['B'+str(roe)] = listo.iloc[i,1]
            roe=roe+1
        
        workbook.save(filename=new_final_name)
        



    #LAST OPERATION:
            
    with open('./'+new_final_name, "rb") as file:
            bot.send_document(idd,document=file)
    update.message.reply_text("Your report is ready ✔")

    os.remove('./'+new_final_name)

    phname = update.message.chat.first_name
    print("Successful Report for: " + phname)
    print("link: " + url)
    print("Total profiles: " + str(total_profiles))
    print("New profiles: " + str(total_new_profiles))
    print("Updated profiles: " + str(total_updated_profiles))   
    print("_______________________________________________________") 
    bot.send_message(chat_id=602083684, text = "---" + name + "--- Total profiles is: " + str(total_profiles))  


def main():
    print("Started")
    

    dp.add_handler(CommandHandler("start", start_command))
    dp.add_handler(MessageHandler(Filters.text, link_handler))

    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()